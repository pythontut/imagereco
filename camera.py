import cv2.cv as cv

cap = cv.CaptureFromCAM(0)
cv.NamedWindow("Camera", 1)

while True:
    img = cv.QueryFrame(cap)
    cv.ShowImage("Camera", img)
    if cv.WaitKey(10) == 27:
        break
cv.DestroyAllWindows()

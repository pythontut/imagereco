# Works with Py 3.5 64bit
import numpy as np
import opencv
import skimage
import matplotlib.pyplot as plt
from PIL import Image
#import Image
from skimage import data
from skimage import data, io, filters

camera = data.camera()
print(camera.shape)

i = Image.open('images\dotndot.png')
iar=np.asarray(i)

print(iar.shape)
iss = np.reshape(iar,(8,8,4))
iss.flags.writeable = True
#print(iss)
print(camera)
print(camera.shape)

edges = filters.sobel(camera)
edges1 = filters.denoise_bilateral(camera)
plt.subplot(221)
plt.imshow(camera)
plt.subplot(222)
plt.imshow(edges)
plt.subplot(223)
plt.imshow(edges1)

mask = iss[:7] > 87
iss[mask] = 0

#plt.imshow(camera)

#plt.imshow(iss)
plt.show()

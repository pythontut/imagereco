import numpy as np   
import matplotlib.pyplot as pyplot
import numpy as np

x = np.array([[1,2,3,4,5],[2,4,6,4,3]], np.int32)
y = x**3
pyplot.plot(x,y, color='green', linestyle='dashed', linewidth=5.0)
pyplot.legend(title='The legend')
pyplot.line = pyplot.plot(x,y, label='x cubed')
pyplot.legend(loc='upper left')
pyplot.plot(x,y)
pyplot.show()
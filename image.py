import numpy as np
import cv2
import matplotlib.pyplot as plt
import mahotas as mh
dna = mh.imread('dna.jpeg')
plt.gray()
dnaf = mh.gaussian_filter(dna,8)
T = mh.thresholding.otsu(dnaf,True)
plt.imshow(dnaf >T)
plt.show()
